## Bridge Network Role

This is a role to create a bridge network on my workstations. This allows my
VMs to use the bridge for their network interface, so that they are on the same
subnet in the network as my hardware machines.

## Requirements

- Configured to run Ansible playbooks
    - SSH setup
    - Password-less `sudo` configured
- Currently, only supports  RHEL-based Distros (Fedora/CentOS/RHEL)

## Known Issues/Quirks

- The system will usually have to be rebooted for the bridge to properly
    startup fully.

## Variables

| Variable | Description | Default/Required |
|----------|-------------|---------------|
| `bridge_name` | The name of the bridge | Defaults to `br01` |
| `bridge_device` | The network device name for the bridge | Defaults to `br01` |
| `bridge_config_filename` | The name of the bridge configuration file saved under the network-scripts | Defaults to `ifcfg-{{ bridge_name }}` |
| `bridge_dns1` | The DNS server the bridge should point to. | **Required** |
| `bridge_ip` | The ip address for network bridge. | **Required** |
| `bridge_gateway` | The gateway address for network bridge to point to. | **Required** |
| `network_device` | The network device to link the bridge to. (ex: `eth1` | **Required** |
| `network_device_filename` | The name of the network device configuration file saved under the network-scripts | Defaults to `ifcfg-{{ network_device }}` |
