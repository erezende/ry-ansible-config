## Hack Fonts

This role will down load the [Hack font
package](https://sourcefoundry.org/hack/), extract it to the user's local font
directory, and update the font cache (fc-cache).

## Requirements

- Server configured for ansible
    - SSH setup
    - Password-less `sudo` configured
    - If running on local machine, ansible installed
- An internet connection to pull package
- fc-cache (not sure if this is distro/DE specific or anything...)

## Variables

| Variable | Description | Default/Required |
|----------|-------------|---------------|
| `user` | The user to run everything under | Defaults to `ansible_user_id` |
| `font_package_src` | The url of the font package to extract and install | Defaults to Hack v3 download |
| `font_extract_dir` | The directory to extract the hack fonts to | Defaults to `/home/{{ user }}/.local/share/fonts/hack` |

