## Nativefier Role

This role will manage spinning up and down a minecraft server container using podman.

## Requirements

- Server configured for ansible
    - Password-less `sudo` configured
    - If running on local machine, ansible installed
- Podman

## Variables

| Variable | Description | Default/Required |
|----------|-------------|---------------|
| `user` | The user to run everything under. | Defaults to `ansible_user_id` |
| `nativefier_dir` | The directory build the nativefier apps at. | Defaults to `nativefier_dir` |
| `nativefier_install_dir`| The directory to move the built nativefier apps to. Will be created if it does not exist. | Defaults to `/home/{{ user }}/Builds/` |
| `icons_dir`| The directory to place the application icons. | Defaults to `/home/{{ user }}/.local/share/icons/` ||
| `applicatons_dir`| The directory to place the `.desktop` application files. | Defaults to `/home/{{ user }}/.local/share/applications/` |
| `nativefier_apps`| A list of the applications to create with nativefier. See
below for examples. | **Required** |

## Nativefier Apps Variable

As mentioned above, the `nativefier_apps` variable is a list of items. Each
item in the list defines a website to turn into a nativefier application. Each
app item has three values to define:


| Variable | Description |
|----------|-------------|
| `name` | The name of the application. This will be used for app and file names. |
| `icon` | The icon filename to use for the application. The file *must* be located in the `./files/` directory of this role. |
| `url` | The url to turn into a 'webapp' using nativefier |

### Example:

For example, here is a set defining two applications:

```
nativefier_apps:
  - name: pocketcasts
    icon: pocketcasts.png
    url: "https://play.pocketcasts.com/"
  - name: pocket
    icon: pocket.png
    url: "https://app.getpocket.com"
```

## Other

Just note, not every website will work with this.
