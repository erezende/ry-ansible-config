## Podman-MC Role

This role will manage spinning up and down a minecraft server container using podman.

## Requirements

- Server configured for ansible
    - Password-less `sudo` configured
    - If running on local machine, ansible installed
- Podman

## Variables

| Variable | Description | Default/Required |
|----------|-------------|---------------|
| `user` | The user to run everything under | Defaults to `ansible_user_id` |
| `container_name` | The name of the mc container to be created (Note will be deleted) | Defaults to `mc-podman` |
| `local_minecraft_dir` | The directory to mount the container's data volume. The server's files will be accessable here. | Defaults to `/tmp/minecraft` |
| 'pre_clean' | When `True`, will delete and clear out the `local_minecraft_dir` before starting up the container | Defaults to `False` |
| 'post_clean' | When `True`, will delete and clear out the `local_minecraft_dir` after tearing down the container | Defaults to `False` |
| `world_to_load` | A path to a minecraft world folder, that when provided will be copied to the `local_miencraft_dir` for the server to load| Defaults to none provided |
| `save_world_dir` | A path that when provided, will copy a backup of the world to before shutting down sthe container | Defaults to none provided |
| `save_world_name` | The name of the world to save at `save_world_dir` | Defaults to `world` (Be careful!) |
| `timestamp_save_world` | When `True`, will append a timestamp to the `save_world_name` name saved at `save_world_dir`.| Defaults to `False` |
| `archive_save_World` | When `True`, will compress the backup dir. (Note, cannot be used as a `world_to_load` dir then) | Defaults to `False` |
| `archive_type` | When `archive_save_world` is `True`, defines the type of compression archive. | Defaults to `gz` |
| `mc_port` | The port to run forward the server to. This port will also be opened (or closed when bringing down the container) using `firewalld`. | Defaults to `25565` |
| `mc_server_action` | The action executing the role does. `up` configures and runs the tasks to load and start the server. `down` (optionally) backups/archives the server, shuts it down, and closes the firewall port. `backup` copies the server's world to the backup location. | **Required**: Must be provided |


## Playbook Examples

To use this role, just define the desired variables which differ from the
defaults, and include the role.

### Up
For example, a playbook to bring up the server:

```yaml
---
- name: Setup and start a MC server in podman
  hosts: "{{ run_on_host | default('127.0.0.1') }}"

  vars:
    user: "ryan"
    mc_server_action: "up"
    local_minecraft_dir: "/tmp/minecraft"
    world_to_load: "/home/ryan/minecraft/backups/survival1/"

  roles:
    - apps/podman-mc
```

This particular playbook will startup the server, copy a saved world located at
`/home/ryan/minecraft/backups/survival1/` to the container's working volume at
`/tmp/minecraft/`. This will load that saved world so it is playable in the
container server.

### Down

Likewise, an example playbook to bring down the server:

```yaml
- name: Backup and bring down podman MC server
  hosts: "{{ run_on_host | default('127.0.0.1') }}"

  vars:
    user: "ryan"
    mc_server_action: "down"
    local_minecraft_dir: "/tmp/minecraft"
    save_world_dir: "/home/ryan/minecraft/backups/"
    save_world_name: "survival1"

  roles:
    - apps/podman-mc
```

This will take server's running world from the container's volume location
(`/tmp/minecraft`) and back it up to the directory
`/home/ryan/minecraft/backups` with the folder name `survival1`.

Additionally, if I wanted to *archive* my worlds every time I brought the
server down, I could add the var `timestamp_save_world: True`, and it will save
the world with a timestamp in the folder name (ex:
`survival1-2020-09-05-11-11-06`).

Lastly, I could also add the var `archive_save_world" True` and/or
`archive_type: "zip"` to save it as a timestamped package instead of an
uncompressed folder.


```yaml
- name: Backup and bring down podman MC server
  hosts: "{{ run_on_host | default('127.0.0.1') }}"

  vars:
    user: "ryan"
    mc_server_action: "down"
    local_minecraft_dir: "/tmp/minecraft"
    save_world_dir: "/home/ryan/minecraft/backups/"
    save_world_name: "survival1"
    timestamp_save_world: True
    archive_save_world: True
    archive_type: "zip"

  roles:
    - apps/podman-mc
```

### Backup
For example, a playbook to backup a running server:

```yaml
---
- name: Backup podman MC server
  hosts: "{{ run_on_host | default('127.0.0.1') }}"
  #connection: local

  vars:
    user: "ryan"
    mc_server_action: "down"
    local_minecraft_dir: "/tmp/minecraft"
    save_world_dir: "/home/ryan/Builds/minecraft/backups"
    save_world_name: "survival"
    timestamp_save_world: True
    archive_save_world: True

  roles:
    - apps/podman-mc
```

This particular playbook will copy the running world located at
`/tmp/minecraft/` to the backup location at
`/home/ryan/minecraft/backups/survival1/`. The `timestamp_save_world` and
`active_save_world` are both `True`, so the backup will be a time-stamped `.gz`
package.
