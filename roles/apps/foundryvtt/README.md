## Foundry VTT role

This role will setup a server to run [foundryvtt](https://foundryvtt.com),
install it, create a systemd service file for it, and start the service.

## Requirements

- Server configured for ansible
    - SSH setup
    - Password-less `sudo` configured
    - If running on local machine, ansible installed
- The downloaded foudryvtt `.zip` package to provide the playbook

## Variables

| Variable | Description | Default/Required |
|----------|-------------|---------------|
| `foundryvtt_send_src` | The location on the machine running the playbook, of the downloaded foundryvtt zip package| **Required**, must be provided |
| `user` | The user to run everything under | Defaults to `ansible_user_id` |
| `foundryvtt_dir` | The directory to unpackage and install foundryvtt to | Defaults to `/home/{{ user }}/foundryvtt/` |
| `foundrydata_dir` | The directory to store foundryvtt data | Defaults to `/home/{{ user }}/foundrydata` |

